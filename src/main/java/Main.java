import com.tsystems.javaschool.tasks.calculator.Calculator;
import com.tsystems.javaschool.tasks.pyramid.PyramidBuilder;
import com.tsystems.javaschool.tasks.subsequence.Subsequence;


import java.util.Arrays;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;


public class Main {
    public static void main(String[] args) {

        //subsequence
        System.out.println("~~~Subsequence task~~~");
        Subsequence s = new Subsequence();

        List x = Stream.of(1, 3, 5, 7, 9).collect(toList());
        List y = Stream.of(10, 1, 2, 3, 4, 3, 5, 7, 9, 20).collect(toList());

        System.out.println("x: " + x.toString());
        System.out.println("y: " + y.toString());
        //run
        System.out.println("result: " + s.find(x, y));


        //calculator
        System.out.println("\n~~~Calculator task~~~");
        Calculator calculator = new Calculator();
        System.out.println("input: (-10.5)/5");
        System.out.println("result: " + calculator.evaluate("(-10.5)/5"));


        //pyramid
        System.out.println("\n~~~Pyramid task~~~");
        PyramidBuilder pyramidBuilder = new PyramidBuilder();

        List<Integer> input = Arrays.asList(1, 3, 2, 9, 4, 5);
        System.out.println("input: " + input.toString());
        // run
        pyramidBuilder.buildPyramid(input);
        System.out.println("result:");
        pyramidBuilder.printPyramid();

    }
}
