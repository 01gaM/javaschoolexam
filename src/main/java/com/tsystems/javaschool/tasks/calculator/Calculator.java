package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */


    public String evaluate(String statement) {
        // TODO: Implement the logic here
        //check input format
        if (statement == null || statement.isEmpty() || !checkInputFormat(statement)) return null;

        StringBuilder statementBuilder = new StringBuilder(statement);

        while (statementBuilder.toString().contains("(")) {
            int startIndex = getStartIndex(statementBuilder.toString());
            int endIndex = statementBuilder.indexOf(")");
            String res = count(statementBuilder.substring(startIndex + 1, endIndex));
            if (res == null) return null;
            statementBuilder.replace(startIndex, endIndex + 1, res);
        }

        String countRes = count(statementBuilder.toString());
        if (countRes == null) return null;

        Double finalRes = Double.parseDouble(countRes);
        if (finalRes % 1 == 0) {
            return Integer.toString(finalRes.intValue());
        }
        return Double.toString(Math.round(finalRes * 10000) / 10000.0);
    }

    private String count(String input) {
        //split by numbers and operations
        String[] numbers;
        String[] operations;
        if (input.charAt(0) == '-') {
            numbers = input.substring(1).split("[+\\-/*]");
            operations = input.substring(1).split("[\\d]+");
            numbers[0] = '-' + numbers[0];
        } else {
            numbers = input.split("[+\\-/*]");
            operations = input.split("[\\d]+");
        }
        ArrayList<String> operationsList = new ArrayList<>(Arrays.asList(operations));
        operationsList.removeAll(Arrays.asList("", "."));
        operationsList.replaceAll(x -> x.substring(0, 1)); //remove "-" sign from other operations

        //Parse Double values
        ArrayList<Double> numbersList = new ArrayList<>();
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i].isEmpty()) {
                numbers[i + 1] = "-" + numbers[i + 1];
                continue;
            }
            try {
                numbersList.add(Double.parseDouble(numbers[i]));
            } catch (Exception e) {
                return null;
            }
        }

        //multiplication and division first
        while (operationsList.contains("/")) {
            int operatorIndex = operationsList.indexOf("/");
            if (numbersList.get(operatorIndex + 1) == 0) return null; //division by zero
            double res = numbersList.get(operatorIndex) / numbersList.get(operatorIndex + 1);
            numbersList.set(operatorIndex, res);
            numbersList.remove(operatorIndex + 1);
            operationsList.remove(operatorIndex);
        }

        while (operationsList.contains("*")) {
            int operatorIndex = operationsList.indexOf("*");
            double res = numbersList.get(operatorIndex) * numbersList.get(operatorIndex + 1);
            numbersList.set(operatorIndex, res);
            numbersList.remove(operatorIndex + 1);
            operationsList.remove(operatorIndex);
        }

        //+ and - at the end
        double res = numbersList.get(0);
        for (int i = 0; i < operationsList.size(); i++) {
            if (operationsList.get(i).equals("+")) {
                res += numbersList.get(i + 1);
            } else {
                res -= numbersList.get(i + 1);
            }
        }
        return Double.toString(res);
    }

    //returns a statement in the deepest parenthesis
    private int getStartIndex(String input) {
        int startIndex = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == '(') {
                startIndex = i;
            } else if (input.charAt(i) == ')') {
                break;
            }
        }
        return startIndex;
    }

    private boolean checkParentheses(String input) {
        int openingParCount = 0;
        for (char el : input.toCharArray()) {
            if (el == '(') {
                openingParCount++;

            } else if (el == ')') {
                if (openingParCount == 0) {
                    return false;
                }
                openingParCount--;
            }
        }
        return openingParCount == 0;
    }


    private boolean checkInputFormat(String input) {
        Pattern patMinus1 = Pattern.compile("[^\\d)(]*[\\-][^\\d(]+");
        Matcher minus1 = patMinus1.matcher(input);
        Pattern patMinus2 = Pattern.compile("[^\\d)(]+[\\-][^\\d(]*");
        Matcher minus2 = patMinus2.matcher(input);

        Pattern pat1 = Pattern.compile("[^\\d)]*[+/*][^\\d(]+");
        Matcher m1 = pat1.matcher(input);
        Pattern pat2 = Pattern.compile("[^\\d)]+[+/*][^\\d(]*");
        Matcher m2 = pat2.matcher(input);

        Pattern pat3 = Pattern.compile("\\.[^\\d]+");
        Matcher m3 = pat3.matcher(input);
        Pattern pat4 = Pattern.compile("[^\\d]+\\.");
        Matcher m4 = pat4.matcher(input);

        return checkParentheses(input) && !(m1.find() || m2.find() || m3.find() || m4.find() || minus1.find() || minus2.find());
    }

}
