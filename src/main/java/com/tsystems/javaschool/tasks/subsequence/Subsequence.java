package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */

    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null) throw new IllegalArgumentException("Can't use null values in this function");
        if (x.isEmpty()) return true;
        if (x.isEmpty() ^ y.isEmpty() || x.size() > y.size()) return false;

        ArrayList<Object> yArr = new ArrayList(y);
        int xIndex = 0;
        int yIndex = 0;
        while (yArr.size() != x.size() && yIndex < yArr.size() && xIndex < x.size()) {
            Object currEl = x.get(xIndex);
            if (yArr.contains(currEl)) {
                //remove all elements before current X element
                while (!yArr.get(yIndex).equals(currEl)) {
                    yArr.remove(yIndex);
                }
            } else {
                return false;
            }
            xIndex++;
            yIndex++;
        }
        //remove any objects left at the end
        while (yArr.size() != x.size()) {
            yArr.remove(yArr.size() - 1);
        }
        return x.equals(yArr);
    }
}
